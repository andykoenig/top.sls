{# call me with `sudo salt-call --local  state.sls test pillar='{"data":"I was a pillar"}'` #}

dump_pillar:
  file.serialize:
    - name: /tmp/pillar-data.yaml
    - formatter: yaml
    - dataset_pillar: data